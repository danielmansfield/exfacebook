daniel.mansfield@mail.huji.ac.il

by Daniel Mansfield

Ex - bookFace

Files:

index.html - registration page
main.html - message board
login.html - login page
png, svg images
user.json - list of online users
messages.json - messages

css file courtesy of http://www.csszengarden.com/
send image: http://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Send-email.svg/750px-Send-email.svg.png
comment image: http://www.guildford-dragon.com/wp-content/uploads/2012/09/comment.png
